var net = require('net'),
  JsonSocket = require('json-socket');
// event from client
var port = 9838;
// var port = 31000;
// var host = '192.168.31.101';
var server = net.createServer();
server.listen(port);
console.log('Waiting for a connection');
server.on('connection', function (socket) {
  console.log('conecting');
  socket = new JsonSocket(socket);
  socket.on('message', function (message) {
    if (message.command == 'auth') {
      auth(socket, message);
    } else if (message.command == 'registr') {
      registr(socket, message);
    } else if (message.command == 'ticket_purchase') {
      purchase(socket, message);
    } else if (message.command == 'load-trains') {
      loadTrains(socket);
    } else if (message.command == 'load_data') {
      loadData(socket, message);
    } else if (message.command == 'ticket_list') {
      ticketList(socket, message);
    } else if (message.command == 'reserve_list') {
      reserveList(socket, message);
    } else if (message.command == 'ticket_reserve') {
      reserve(socket, message);
    }
  });
});

//DATABASE
const mysql = require('mysql2');
const connection = mysql.createPool({
  host: 'localhost',
  user: 'mysql',
  database: 'train',
  password: 'mysql',
});

function auth(socket, message) {
  const sql = `SELECT*FROM users WHERE login='${message.login}' AND password='${message.password}'`;
  connection.query(sql, function (err, results) {
    if (err) {
      console.log(err);
    } else if (results.length == 0) {
      console.log('Такого логіна або пароля не існує');
      socket.sendMessage({ command: 'auth_failed' });
    } else {
      console.log('Користувач -' + message.login);
      socket.sendMessage({ command: 'auth_success', user: results[0] });
    }
  });
}

function registr(socket, message) {
  const sql = `SELECT login FROM users WHERE login='${message.login}'`;
  connection.query(sql, function (err, results) {
    if (err) {
      console.log(err);
    } else if (results.length == 0) {
      console.log(message.login);
      const sql = `INSERT INTO users VALUES(null, '${message.login}', '${message.password}', '${message.firstName}', '${message.lastName}', '${message.email}')`;
      connection.query(sql, function (err, results) {
        if (err) {
          console.log(err);
        } else {
          console.log('Користувач зареєстрований');
          socket.sendMessage({ command: 'registr_success' });
        }
      });
    } else {
      console.log(results + ' ' + results.length);
      socket.sendMessage({ command: 'registr_failed' });
    }
  });
}

function purchase(socket, message) {
  const sql = `SELECT * FROM purchased_tickets WHERE route='${message.route}' AND date_time ='${message.date_time}' AND car='${message.car}' AND place='${message.place}'AND reserved_car is NULL AND reserved_place is NULL`;
  connection.query(sql, function (err, results) {
    if (err) {
      console.log(err);
    } else if (results.length == 0) {
      console.log(results.length);
      const sql = `INSERT INTO purchased_tickets VALUES('${message.login}', '${message.route}', '${message.date_time}', '${message.car}', '${message.place}',NULL, NULL)`;
      connection.query(sql, function (err, results) {
        if (err) {
          console.log(err);
        } else {
          console.log(results);
          console.log('Квиток зареєстрований');
          socket.sendMessage({ command: 'purchase_success' });
        }
      });
    } else {
      console.log(results.length + '-Неможливо купити квиток');
      socket.sendMessage({ command: 'purchase_failed' });
    }
  });
}

function reserve(socket, message) {
  const sql = `SELECT * FROM purchased_tickets WHERE route='${message.route}' AND date_time ='${message.date_time}' AND car IS NULL AND place is NULL AND reserved_car='${message.car}' AND reserved_place='${message.place}'`;
  connection.query(sql, function (err, results) {
    if (err) {
      console.log(err);
    } else if (results.length == 0) {
      console.log(results.length);
      const sql = `INSERT INTO purchased_tickets VALUES('${message.login}', '${message.route}', '${message.date_time}',NULL, NULL, '${message.car}', '${message.place}')`;
      connection.query(sql, function (err, results) {
        if (err) {
          console.log(err);
        } else {
          console.log(results);
          console.log('Квиток зареєстрований');
          socket.sendMessage({ command: 'reserve_success' });
        }
      });
    } else {
      console.log(results.length + '-Неможливо забронювати квиток');
      socket.sendMessage({ command: 'purchase_failed' });
    }
  });
}

function loadTrains(socket) {
  const sql = `SELECT * FROM trains GROUP BY route`;
  connection.query(sql, function (err, results) {
    if (err) {
      console.log(err);
    } else {
      socket.sendMessage({ command: 'load_train_success', trains: results });
    }
  });
}

function loadData(socket, message) {
  const sql = `SELECT * FROM trains  WHERE route = '${message.route}'`;
  connection.query(sql, function (err, results) {
    if (err) {
      console.log(err);
    } else {
      socket.sendMessage({ command: 'load_data_success', data: results });
    }
  });
}

function ticketList(socket, message) {
  const sql = `SELECT * FROM purchased_tickets  WHERE login = '${message.login}' AND reserved_car is NULL`;
  connection.query(sql, function (err, results) {
    if (err) {
      console.log(err);
    } else {
      socket.sendMessage({ command: 'ticket_list_success', data: results });
    }
  });
}

function reserveList(socket, message) {
  const sql = `SELECT * FROM purchased_tickets  WHERE login = '${message.login}' AND car is NULL`;
  connection.query(sql, function (err, results) {
    if (err) {
      console.log(err);
    } else {
      socket.sendMessage({ command: 'reserve_list_success', data: results });
    }
  });
}
